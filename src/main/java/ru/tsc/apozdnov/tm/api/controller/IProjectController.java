package ru.tsc.apozdnov.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject();

    void removeByIndex();

    void removeProjectById();

    void updateByIndex();

    void updateById();

    void showByIndex();

    void showById();

    void changeStatusById();

    void changeStatusByIndex();

    void startById();

    void startByIndex();

    void completeById();

    void completeByIndex();

}
