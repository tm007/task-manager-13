package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    Task remove(Task task);

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAllByProjectId(final String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    void clear();

}
