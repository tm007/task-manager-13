package ru.tsc.apozdnov.tm.controller;

import ru.tsc.apozdnov.tm.api.controller.IProjectController;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.api.service.IProjectTaskService;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjectList() {
        System.out.println("**** PROJECTS LIST ****");
        final List<Project> projects = projectService.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
        System.out.println("**** OK ****");
    }

    @Override
    public void clearProjects() {
        System.out.println("**** PROJECT CLEAR ****");
        projectService.clear();
        System.out.println("**** CLEARED ****");
    }

    @Override
    public void createProject() {
        System.out.println("**** PROJECT CREATE ****");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("**** FAULT ****");
        else System.out.println("**** PROJECT CREATED ****");
    }

    @Override
    public void removeByIndex() {
        System.out.println("***** REMOVE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("****FAULT****");
            return;
        }
        projectTaskService.removeProjectById(project.getId());
        System.out.println("OK");
    }

    @Override
    public void removeProjectById() {
        System.out.println("***** REMOVE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final boolean flag = projectTaskService.removeProjectById(projectId);
        if (!flag) System.out.println("****FAULT****");
        else System.out.println("OK");
    }

    @Override
    public void updateByIndex() {
        System.out.println("***** UPDATE PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer id = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(id, name, description);
        if (project == null) System.out.println("****FAULT****");
        else System.out.println("OK");
    }

    @Override
    public void updateById() {
        System.out.println("***** UPDATE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        if (project == null) System.out.println("****FAULT****");
        else System.out.println("OK");
    }

    @Override
    public void showByIndex() {
        System.out.println("***** SHOW PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("****FAULT****");
            return;
        } else {
            showProject(project);
            System.out.println("OK");
        }
    }

    @Override
    public void showById() {
        System.out.println("***** SHOW PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("****FAULT****");
            return;
        }
        showProject(project);
        System.out.println("OK");
    }

    @Override
    public void changeStatusById() {
        System.out.println("***** CHANGE PROJECT STATUS BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) System.out.println("****FAIL****");
        else System.out.println("****OK****");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("***** CHANGE PROJECT STATUS BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) System.out.println("****FAIL****");
        else System.out.println("****OK****");
    }

    @Override
    public void startById() {
        System.out.println("***** START PROJECT BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.IN_PROGRESS);
        if (project == null) System.out.println("****FAIL****");
        else System.out.println("****OK****");
    }

    @Override
    public void startByIndex() {
        System.out.println("***** START PROJECT BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) System.out.println("****FAIL****");
        else System.out.println("****OK****");
    }

    @Override
    public void completeById() {
        System.out.println("***** COMPLETE PROJECT BY ID ****");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.COMPLETED);
        if (project == null) System.out.println("****FAIL****");
        else System.out.println("****OK****");
    }

    @Override
    public void completeByIndex() {
        System.out.println("***** COMPLETE PROJECT BY INDEX ****");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeStatusByIndex(index, Status.COMPLETED);
        if (project == null) System.out.println("****FAIL****");
        else System.out.println("****OK****");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescriprion());
        System.out.println("ID: " + project.getId());
        final Status status = project.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
    }

}
