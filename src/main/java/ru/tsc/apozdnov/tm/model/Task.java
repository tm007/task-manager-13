package ru.tsc.apozdnov.tm.model;

import ru.tsc.apozdnov.tm.enumerated.Status;

import java.util.UUID;

public final class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String descriprion = "";

    private Status status = Status.NOT_STARTED;

    private String projectId;

    public Task() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriprion() {
        return descriprion;
    }

    public void setDescriprion(String descriprion) {
        this.descriprion = descriprion;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "ID: " + id + " " +
                "NAME" + ": " + name + "  " +
                "DESCRIPTION: " + ": " + descriprion + "  " +
                "PROJECTID:" + projectId + "\n";
    }

}
